import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  texto = 'Um longo texto para ser truncado.';
  value = 12345678.9;
  now = new Date();
}
