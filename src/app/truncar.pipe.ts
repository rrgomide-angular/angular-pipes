import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncar'
})
export class TruncarPipe implements PipeTransform {
  transform(value: string, minSize = 5, symbol = '...'): any {
    return value.substring(0, minSize) + symbol;
  }
}
