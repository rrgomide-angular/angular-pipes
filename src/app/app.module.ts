import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TruncarPipe } from './truncar.pipe';

/**
 * Importando o locale brasileiro
 */
import localeBrazil from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';

registerLocaleData(localeBrazil);

@NgModule({
  declarations: [AppComponent, TruncarPipe],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
